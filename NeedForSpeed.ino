#include <ir_Lego_PF_BitStreamEncoder.h>
#include <boarddefs.h>
#include <IRremoteInt.h>
#include <IRremote.h>

#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//From bildr article: http://bildr.org/2012/08/rotary-encoder-arduino/

#include "Wire.h" // For I2C
#include "LCD.h" // For LCD
#include "LiquidCrystal_I2C.h" // Added library*
//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the default I2C bus address of the backpack-see article
const int RECV_PIN = 7;
IRrecv irrecv(RECV_PIN);
decode_results results;
int x = 0;
int y = 0;

long per = 0;
int frek = 300;
long score = 0;

class Obstacle {
  public: 
  int oX;
  int oY;

  Obstacle(){};
  Obstacle(int x, int y) {
    oX = x;
    oY = y;
  }

  void moveObstacle() {
    lcd.setCursor(oX, oY);
    lcd.print("    ");
    oX--;
    if(oX == -1) {
      oX = 20;    
    }
  }

  boolean collidesWithCar(int x, int y) {
    if(x == oX && y == oY) {
      return true;
    }
    return false;
  }

  void printObstacle() {
    lcd.setCursor(oX, oY);
    lcd.print("0");
  }
};

Obstacle *obstacle;
Obstacle *obstacle1;
Obstacle *obstacle2;
Obstacle *obstacle3;
boolean gamingTime = false;
void setup() {
  lcd.begin (16, 2); // 16 x 2 LCD module
  lcd.setBacklightPin(3, POSITIVE); // BL, BL_POL
  lcd.setBacklight(HIGH);
  lcd.home();
  irrecv.enableIRIn();
  irrecv.blink13(true);
  Serial.begin(9600);
  x = 0;
  y = 0;
  printCar(x, y);
  score = 0;
  obstacle = new Obstacle(12, 0);
  obstacle->printObstacle();
  obstacle1 = new Obstacle(17, 1);
  obstacle1->printObstacle();
  obstacle2 = new Obstacle(22, 0);
  obstacle2->printObstacle();
}

void loop() {
  if(irrecv.decode(&results)) {
    if(results.value == 16769055) {
      buttonUpPressed();
    } else if (results.value == 16748655) {
      buttonDownPressed();
    } else if (results.value == 16753245) {
      gamingTime = true;
    }
    irrecv.resume();
    printCar(x, y);
  }

  if(gamingTime) {
    if(millis() >= per) {
    per += frek;
    obstacle->moveObstacle();
    obstacle->printObstacle();
    if(obstacle->collidesWithCar(x, y)) {
      gameOver();
    }
    obstacle1->moveObstacle();
    obstacle1->printObstacle();
    if(obstacle1->collidesWithCar(x, y)) {
      gameOver();
    }
    obstacle2->moveObstacle();
    obstacle2->printObstacle();
    if(obstacle2->collidesWithCar(x, y)) {
      gameOver();
    }
    score = score + (1 * millis() / 1000);
    Serial.println(score);
    }
  }
  
}

void buttonUpPressed() {
  if(y == 0) {
    y++;
  }
}

void buttonDownPressed() {
  if(y == 1) {
    y--; 
  }
}

void gameOver() {
  gamingTime = false;
      lcd.home();
      lcd.print(score);
      per = millis();
      score = 0;
}

void printCar(int x, int y) {
  lcd.home();
  lcd.print("   ");
  lcd.setCursor(0, 1);
  lcd.print("   ");
  lcd.setCursor(x, y);
  lcd.print(">");
}
